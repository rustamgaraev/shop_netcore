﻿namespace Shop_NetCore.Data.Static
{
    public static class StaticCategory
    {
        /// <summary>
        /// Легковые автомобили.
        /// </summary>
        public static string PassengerCars = "Легковые автомобили";

        /// <summary>
        /// Грузовые автомобили.
        /// </summary>
        public static string TruckCars = "Грузовые автомобили";
    }
}
