﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Shop_NetCore.Data.Models;
using Shop_NetCore.Data.Static;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop_NetCore.Data
{
    public static class DbObjects
    {
        public static void Initial(IApplicationBuilder applicationBuilder)
        {
            using (var scope = applicationBuilder.ApplicationServices.CreateScope())
            {
                AppDBContent dBContent = scope.ServiceProvider.GetRequiredService<AppDBContent>();
                // добавим категорию если ее нет
                if (!dBContent.Category.Any())
                {
                    dBContent.Category.AddRange(Categories.Select(x => x.Value));
                }

                // добавим товар если нет
                if (!dBContent.Car.Any())
                {
                    dBContent.AddRange(
                         new Car
                         {
                             Name = "Mazda 3",
                             Description = "Mazda 3",
                             ImageUrl = "/img/car1.jpg",
                             Price = 700000.00M,
                             Category = Categories[StaticCategory.PassengerCars]
                         },
                        new Car
                        {
                            Name = "BMW X5",
                            Description = "bmw x5",
                            ImageUrl = "/img/car2.jpg",
                            Price = 1200000.00M,
                            Category = Categories[StaticCategory.PassengerCars]
                        },
                        new Car
                        {
                            Name = "Газель",
                            Description = "Газель",
                            ImageUrl = "/img/car3.jpg",
                            Price = 1000000.00M,
                            Category = Categories[StaticCategory.TruckCars]
                        }
                        );
                }

                dBContent.SaveChanges();
            }
        }

        private static Dictionary<string, Category> _Category;

        public static Dictionary<string, Category> Categories
        {
            get
            {
                if (_Category == null)
                {
                    var listCategory = new Category[]
                    {
                        new Category { Name = StaticCategory.PassengerCars, Description = StaticCategory.PassengerCars },
                        new Category { Name = StaticCategory.TruckCars, Description = StaticCategory.TruckCars }
                    };

                    _Category = new Dictionary<string, Category>();
                    foreach (var category in listCategory)
                    {
                        _Category.Add(category.Name, category);
                    }
                }

                return _Category;
            }
        }
    }
}
