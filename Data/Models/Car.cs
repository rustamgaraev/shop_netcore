﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Shop_NetCore.Data.Models
{
    public class Car
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string ImageUrl { get; set; }

        [Column(TypeName = "decimal(18, 2)")]
        public decimal? Price { get; set; }

        public int CategoryID { get; set; }

        public virtual Category Category { get; set; }
    }
}
