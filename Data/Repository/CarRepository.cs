﻿using Microsoft.EntityFrameworkCore;
using Shop_NetCore.Data.Interfaces;
using Shop_NetCore.Data.Models;
using System.Collections.Generic;
using System.Linq;

namespace Shop_NetCore.Data.Repository
{
    public class CarRepository : ICars
    {
        private readonly AppDBContent _appDBContent;

        public CarRepository(AppDBContent appDBContent)
        {
            _appDBContent = appDBContent;
        }

        public IEnumerable<Car> GetAllCars => _appDBContent.Car.Include(x => x.Category);

        public Car GetCarByID(int carID) => _appDBContent.Car.FirstOrDefault(x => x.ID == carID);
    }
}
