﻿using Shop_NetCore.Data.Interfaces;
using Shop_NetCore.Data.Models;
using System.Collections.Generic;

namespace Shop_NetCore.Data.Repository
{
    public class CategoryRepository : ICarsCategory
    {
        private readonly AppDBContent _appDBContent;

        public CategoryRepository(AppDBContent appDBContent)
        {
            _appDBContent = appDBContent;
        }

        public IEnumerable<Category> GetAllCategories => _appDBContent.Category;
    }
}
