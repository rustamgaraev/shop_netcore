﻿using Shop_NetCore.Data.Interfaces;
using Shop_NetCore.Data.Models;
using System.Collections.Generic;
using System.Linq;

namespace Shop_NetCore.Data.Mocks
{
    public class MockCars : ICars
    {
        private readonly ICarsCategory _carsCategory = new MockCategory();

        public IEnumerable<Car> GetAllCars {
            get
            {
                return new List<Car>
                {
                    new Car
                    {
                        Name = "Mazda 3",
                        Description = "Mazda 3",
                        ImageUrl = "/img/car1.jpg",
                        Price = 700000.00M,
                        Category = _carsCategory.GetAllCategories.First()
                    },
                    new Car
                    {
                        Name = "BMW X5",
                        Description = "bmw x5",
                        ImageUrl = "/img/car2.jpg",
                        Price = 1200000.00M,
                        Category = _carsCategory.GetAllCategories.First()
                    },
                    new Car
                    {
                        Name = "Газель",
                        Description = "Газель",
                        ImageUrl = "/img/car3.jpg",
                        Price = 1000000.00M,
                        Category = _carsCategory.GetAllCategories.Last()
                    }
                };
            }
        }

        public Car GetCarByID(int carID)
        {
            throw new System.NotImplementedException();
        }
    }
}
