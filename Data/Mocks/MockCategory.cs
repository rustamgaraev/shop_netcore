﻿using Shop_NetCore.Data.Interfaces;
using Shop_NetCore.Data.Models;
using System.Collections.Generic;

namespace Shop_NetCore.Data.Mocks
{
    public class MockCategory : ICarsCategory
    {
        public IEnumerable<Category> GetAllCategories
        {
            get {
                return new List<Category>
                {
                    new Category { Name = "Легковые автомобили", Description = "Легковые автомобили" },
                    new Category { Name = "Грузовые автомобили", Description = "Грузовые автомобили" }
                };
            }
        }
    }
}
