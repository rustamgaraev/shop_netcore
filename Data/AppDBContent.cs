﻿using Microsoft.EntityFrameworkCore;
using Shop_NetCore.Data.Models;

namespace Shop_NetCore.Data
{
    public class AppDBContent : DbContext
    {
        public AppDBContent(DbContextOptions<AppDBContent> options) : base(options)
        {

        }

        public DbSet<Car> Car { get; set; }

        public DbSet<Category> Category { get; set; }
    }
}
