﻿using Shop_NetCore.Data.Models;
using System.Collections.Generic;

namespace Shop_NetCore.Data.Interfaces
{
    public interface ICarsCategory
    {
        IEnumerable<Category> GetAllCategories { get; }
    }
}
