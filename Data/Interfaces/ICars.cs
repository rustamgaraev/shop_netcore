﻿using Shop_NetCore.Data.Models;
using System.Collections.Generic;

namespace Shop_NetCore.Data.Interfaces
{
    public interface ICars
    {
        IEnumerable<Car> GetAllCars { get; }

        Car GetCarByID(int carID);
    }
}
