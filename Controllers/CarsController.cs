﻿using Microsoft.AspNetCore.Mvc;
using Shop_NetCore.Data.Interfaces;
using Shop_NetCore.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop_NetCore.Controllers
{
    public class CarsController : Controller
    {
        private readonly ICars _cars;
        private readonly ICarsCategory _carsCategory;

        public CarsController(ICars cars, ICarsCategory carsCategory)
        {
            _cars = cars;
            _carsCategory = carsCategory;
        }

        public ViewResult ListCarsView()
        {
            ViewBag.Title = "Страница с автомобилями";
            CarListViewModel obj = new CarListViewModel();
            obj.GetCars = _cars.GetAllCars;
            obj.CurrentCategory = "Автомобили";
            return View(obj);
        }
    }
}
