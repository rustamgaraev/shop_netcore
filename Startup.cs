using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Shop_NetCore.Data;
using Shop_NetCore.Data.Interfaces;
using Shop_NetCore.Data.Mocks;
using Microsoft.EntityFrameworkCore;
using Shop_NetCore.Data.Repository;

namespace Shop_NetCore
{
    public class Startup
    {
        private IConfigurationRoot _configurationRoot;

        public Startup(IWebHostEnvironment hostEnvironment)
        {
            _configurationRoot = new ConfigurationBuilder().SetBasePath(hostEnvironment.ContentRootPath).AddJsonFile("DBSettings.json").Build();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            IServiceCollection serviceCollection = services.AddDbContext<AppDBContent>(options => options.UseSqlServer(_configurationRoot.GetConnectionString("DefaultConnection")));
            services.AddTransient<ICars, CarRepository>();
            services.AddTransient<ICarsCategory, CategoryRepository>();

            // ��������� ��������� MVC � ������
            services.AddMvc(options => options.EnableEndpointRouting = false);
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            // ��������� � ��������
            app.UseDeveloperExceptionPage();

            app.UseStatusCodePages();
            app.UseStaticFiles();
            app.UseMvcWithDefaultRoute();

            DbObjects.Initial(app);
        }
    }
}
