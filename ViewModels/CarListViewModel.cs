﻿using Shop_NetCore.Data.Models;
using System.Collections.Generic;

namespace Shop_NetCore.ViewModels
{
    public class CarListViewModel
    {
        public IEnumerable<Car> GetCars { get; set; }

        public string CurrentCategory { get; set; }
    }
}
